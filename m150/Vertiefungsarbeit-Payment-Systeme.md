# Vertiefungsarbeit

Vorschlag:
## Paymentsysteme

Diese Arbeit ist sehr geeignet für Lernende in Betrieben wie "SIX" oder anderen Payment-Anbietern sowie Banken oder "Agenturen" die Web-Shops anbieten.

Als eine mögliche Vertiefungsarbeit gibt es den Vorschlag, sich einmal genauer 
 - der ganze Workflow
 - die Varianten
 - die Kosten

der in der Schweiz bestehenden 
 - Zahlungsprovider und/oder
 - Zahlungsmethoden
 
in eBusiness-Applikationen (Web-Shops, Ticket-Systeme) anzuschauen.


Die Ausarbeitung kann Entweder 
 - rein theoretisch (als Gesamtüberblick) oder auch 
 - praktisch als Demo- oder Prototyp wo man sieht, wie man das macht/einbindet und auf welche Probleme man stossen kann

ausgestaltet sein.

<https://www.kmu.admin.ch/kmu/de/home/praktisches-wissen/kmu-betreiben/e-commerce/erstellung-e-commerce-site/online-zahlungsarten.html>

<https://www.teinformatik.ch/post/zahlungsdienstleister-schweizer-online-shops-vergleich>
