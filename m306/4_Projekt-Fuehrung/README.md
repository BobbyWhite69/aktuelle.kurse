# M306/4 Projekt-Führung

## Praxis 
- Offline-Erfahung "Teambildung" inkl. Video-Aufnahmen <br/>
(--> Struktur-Legen in Gruppen zu 5-7 Personen) <br />
Ressourcen: [Struktur-Lege-Karten](../vorlagen_beispiele/M306_StrukturLegeKarten.docx)

- Tabu-Spiel in Gruppen zu 5-7 Personen "[Sitzungstypen](http://www.teamercard.de/fix/files/kd.1126000413/M%203.1.%20Rollen%20in%20Gruppen.2.pdf)" <br/>
(--> Papierstreifen bereitlegen)<br/>


## Theorie
- [Projektführung_Praeinstruktion](./M306_4_Projektfuehrung_Praeinstruktion.txt)


- [Skript (Seiten 23-32)](../docs) 
**"Projektführung"** (entlang dem Theoriedokument)
  - Die Rolle des Projektleiters
  - Teambildung _**( --> Offline-Erfahrung )**_
  - Menschenbild/Sitzungstypen _**( --> Tabu-Spiel )**_
  - Sitzungsgestaltung
  - Pareto-Prinzip (80/20-Regel)
  - Eisenhower-Prinzip (wichtig/dringlich)
  - Entscheidungsfolgen-Matrix

## Anwendung
- PodCast 22:18 min, D, 2020-11-25 - **_Wie ideale Teams funktionieren_** [mp3](https://media.neuland.br.de/file/1810761/c/feed/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.mp3)<br>
[Fragen](./wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.txt) zum PodCast
