### Aufgabe/Task: Nr. 06

Thema: Arrays, Sort, Stack, Queue

Geschätzter Zeitbedarf: 180-240 min

#### Aufgabenbeschreibung:

**Arrays**
<https://www.youtube.com/watch?v=JH8oogtBd4g>

**Stacks**
<https://www.youtube.com/watch?v=wgwLdEg8728>

- [script3_0_usingArrays.pdf](./script3_0_usingArrays.pdf)
- [script3_0_arrays_anwenden_und_sortieren.pdf](./script3_0_arrays_anwenden_und_sortieren.pdf)
- [script3_3_dynamischeStrukturen_stack.pdf](./script3_3_dynamischeStrukturen_stack.pdf)
- [script3_4_dynamischeStrukturen_queue.pdf](./script3_4_dynamischeStrukturen_queue.pdf)

Bewertung: Keine, ist aber prüfungsrelevant
