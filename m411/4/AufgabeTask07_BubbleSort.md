### Aufgabe/Task: Nr. 07

Thema: Bubble-Sort

Geschätzter Zeitbedarf: 90-120 min

#### Aufgabenbeschreibung:

**Bubble Sort**

<https://www.youtube.com/watch?v=XMu1Kq69EhU> (5:32 min)

Machen Sie zuerst die Kapitel 1-3 (unten) durch und bauen Sie ein Bubble-Sort-Algorithmus. Die Daten lesen Sie über eine Liste von
Nummern aus einer Text-Datei ein. Lassen Sie die Veränderungen nach jedem
Durchgang in eine Datei herausschreiben (Zahlenreihe hintereinander und Komma
separiert / Stand vorher eine Zeile, Stand nachher eine Zeile, Stand nachher
eine Zeile, …) damit man sehen kann, wie sich der Algorithmus verhält und wie sich
die Resultate entwickeln.


Bewertung:  Keine, ist aber prüfungsrelevant


### Der Bubblesort

**Lernziele:**

-   Sie haben das Prinzip des Bubblesort verstanden
-   Sie können eine Aussage über die Effizienz des Bubblesorts machen

#### 1.) Sortieren in Schritten

Für die folgenden Aufgaben brauchen Sie Papierschnitzel mit den unten stehenden
Zahlen:

**51, 13, 9, 44, 18, 93, 25**

Schreiben Sie diese Zahlen auf Notizpapier auf und schreiben Sie die Entwicklung des BubbleSort Zeile für Zeile untereinander auf.

##### 1.1) Aufgabe: Sortieren nach Grösse

Sortieren Sie die Papierschnitzel ausgehend von der obigen Reihenfolge der
Grösse nach aufsteigend, so dass die kleinste Zahl links und die grösste rechts
zu liegen kommt.

-   Können Sie beschreiben, wie Sie vorgegangen sind?

##### 1.2) Aufgabe: nur benachbarte Schnipsel vertauschen

Bringen Sie die Papierschnipsel wieder in die Ausgangssituation und sortieren
Sie zum zweiten Mal der Grösse nach aufsteigend. Aber: dieses Mal ist nur eine
**einzige Operation** auf den Papierschnipsel erlaubt, und zwar dürfen Sie nur
jeweils zwei benachbarte Schnipsel vertauschen.

##### 1.3) Aufgabe: systematisch von links nach rechts

Bringen Sie die Papierschnipsel wieder in die Ausgangssituation und sortieren
Sie sie wieder durch Vertauschen von Nachbarn, aber wählen Sie diesmal die
Nachbarn systematisch von links nach rechts. Sie tauschen also – falls nötig –
die erste mit der zweiten Zahl, dann die zweite mit der dritten, usw. bis Sie
beim letzten Paar ganz rechts angekommen sind.

-   Können Sie etwas darüber sagen, was bei einem einzelnen Durchgang passiert?

-   Wann können sie aufhören und brauchen keinen weiteren Durchgang mehr?
    Notieren Sie sich die Zwischenschritte.

|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |

#### 2.) Definition Bubblesort-Algorithmus

Der Bubblesort-Algorithmus sortiert eine Liste von Elementen aufsteigend, indem
er so lange von links nach rechts durch die Liste geht und benachbarte Elemente
vertauscht (falls das linke Element grösser als das rechte ist), bis ein ganzer
Durchgang durch die Liste zu keiner Änderung mehr führt.

Als Flussdiagramm:

![](media/0bf179a2c5f431b207fe5c6804bbcfab.jpg)

#### 3.) Effizienz von Bubblesort

##### 3.1) Anzahl Durchgänge

Überlegen Sie sich, wie viele Durchgänge von links nach rechts maximal nötig
sind, um eine Liste mit *n* Elementen zu sortieren. Sortieren Sie folgende Liste
mit dem Bubblesort-Algorithmus und schreiben Sie sämtliche Zwischenschritte auf:

97, 15, 33, 28, 25, 11, 73

|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |

Wie Sie vermutlich gemerkt haben, genügen *n* Durchgänge. Mit jedem Durchgang
landet mindestens eine der Zahlen an seinem definitivem Platz (es genügen sogar
*n-1* Durchgänge, weil die letzte Zahl keinen Nachbar mehr hat, mit dem sie
vertauscht werden könnte).

##### 3.2) Anzahl Vergleichsoperationen

In einer Liste mit *n* Zahlen gibt es *n*-1 Paare von benachbarten Zahlen, die
bei einem Durchgang verglichen werden müssen. Und wir haben höchstens *n*
Durchgänge. Somit sind *n*(*n*-1) Vergleichsoperationen maximal nötig.

Wir können somit sagen, dass es ungefähr *n2* Vergleichsoperationen für eine
Liste mit n Elementen gibt.

##### 3.3) Aufwand im besten und im schlechtesten Fall

Wenn die Liste bereits sortiert ist, vergleicht der Algorithmus alle *n-1*
benachbarten Zahlenpaare einmal und stellt fest, dass es nichts zu tun gibt.
Damit ist er fertig und es werden keine Zahlen vertauscht. Das ist der beste und
schnellste Fall und benötigt *n-1* Vergleiche und 0 Vertauschungen.

##### 3.4) Aufgabe: der schlechteste Fall**

Überlegen Sie sich die Effizienz im schlechtesten Fall (d.h. wenn eine Liste
absteigend sortiert ist und aufsteigend sortiert werden soll).

|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |

**Lösung**:

Die grösste Zahl steht zu Beginn ganz links und wird im ersten Durchgang in n-1
Vertauschungen ans rechte Ende der Liste gebracht. Die übrigen Elemente der
Liste wandern einen Platz nach links. Jetzt liegt die zweitgrösste Zahl ganz
links und wird im zweiten Durchgang in n-2 Vertauschungen ans rechte Ende der
Liste links der grössten Zahl verschoben. Usw.

Das heisst, wir haben:

*(n-1) + (n-2) + …. + 1 = n(n-1)/2*

Quelle: ETH-Unterlagen, Stand August 2015

