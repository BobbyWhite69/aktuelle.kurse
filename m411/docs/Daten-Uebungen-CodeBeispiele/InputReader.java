import java.io.BufferedInputStream;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * InputReader.
 * Uses stuff from Sedgewick's standard classes.
 * 
 * @author Julian
 *
 */
public class InputReader {
	
	
	private Scanner scanner;
	
	// assume Unicode UTF-8 encoding
    private static final String CHARSET_NAME = "UTF-8";
    
    // assume language = English for consistency with System.out.
    private static final Locale LOCALE = Locale.ENGLISH;
    
    // the default token separator; we maintain the invariant that this value 
    // is held by the scanner's delimiter between calls
    private static final Pattern WHITESPACE_PATTERN
        = Pattern.compile("\\p{javaWhitespace}+");
    
    // used to read the entire input. source:
    // http://weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner_1.html
    private static final Pattern EVERYTHING_PATTERN
        = Pattern.compile("\\A");
	
	
    /**
     * Constructor which also instantiates the Scanner and sets character-set and
     * locale.
     */
	public InputReader(){
		scanner = new Scanner(new BufferedInputStream(System.in), CHARSET_NAME);
        scanner.useLocale(LOCALE);
	}
	
	
	
	
	/**
     * Read and return the next string.
     */
    public String readString() {
        return scanner.next();
    }

   /**
     * Read and return the next int.
     */
    public int readInt() {
        return scanner.nextInt();
    }

   
   

    /**
     * Read and return the next boolean, allowing case-insensitive
     * "true" or "1" for true, and "false" or "0" for false.
     */
    public boolean readBoolean() {
        String s = readString();
        if (s.equalsIgnoreCase("true"))  return true;
        if (s.equalsIgnoreCase("false")) return false;
        if (s.equals("1"))               return true;
        if (s.equals("0"))               return false;
        throw new InputMismatchException();
    }
    
    
    
    /**
     * Read and return the remainder of the input as a string.
     */
    public String readAll() {
        if (!scanner.hasNextLine())
            return "";

        String result = scanner.useDelimiter(EVERYTHING_PATTERN).next();
        // not that important to reset delimeter, since now scanner is empty
        scanner.useDelimiter(WHITESPACE_PATTERN); // but let's do it anyway
        return result;
    }

    
    
    /**
     * Read all strings until the end of input is reached, and return them.
     */
    public String[] readAllStrings() {
        // we could use readAll.trim().split(), but that's not consistent
        // since trim() uses characters 0x00..0x20 as whitespace
        String[] tokens = WHITESPACE_PATTERN.split(readAll());
        if (tokens.length == 0 || tokens[0].length() > 0)
            return tokens;
        String[] decapitokens = new String[tokens.length-1];
        for (int i = 0; i < tokens.length-1; i++)
            decapitokens[i] = tokens[i+1];
        return decapitokens;
    }
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
