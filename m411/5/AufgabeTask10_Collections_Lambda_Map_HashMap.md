### Aufgabe/Task: Nr. 10

Themen: 
- Collections
- Lambda
- Map und HashMap

Geschätzter Zeitbedarf: 90-120 min

#### Aufgabenbeschreibung

Schauen Sie dieses Video an: 
- HashMap Java Tutorial <https://www.youtube.com/watch?v=70qy6_gw1Hc>

Und dann gehen Sie 
[dieser Anleitung nach](./script5_collections_und_lambda.pdf) 
und machen Sie die Dinge nach.

Erweitern Sie hre Kenntnisse mit den [HashMap-Datenstrukturen](./script6_weitereDatenstrukturenHashMap.pdf).
