### Aufgabe/Task: Nr. 09

Thema: HashMaps in Java

Geschätzter Zeitbedarf: 60-120 min

#### Aufgabenbeschreibung:

Schauen Sie ein/zwei der angebotenen Videos über Java HashMap an
- [Videos-Tutorials-Anleitungen](../docs/Videos-Tutorials-Anleitungen)

Und auch noch diese Anleitung:
 - HashMap Java Tutorial (12 min) <https://www.youtube.com/watch?v=70qy6_gw1Hc>
 - **[HashMap als Datenstruktur](./script6_weitereDatenstrukturenHashMap.pdf)** <br>(dafür werden Sie diese Datei [airports.csv](./airports.csv) brauchen)

.. und bauen Sie in Java eine eigene HashMap nach und zeigen Sie es der Lehrperson (evtl. Abgabe).


Bewertung: Keine, ist aber prüfungsrelevant


Weitere Veranschaulichung:
- [hashmap-containsvalue-method-in-java](https://www.geeksforgeeks.org/hashmap-containsvalue-method-in-java/?ref=lbp)
- [hashmap-containskey-method-in-java](https://www.geeksforgeeks.org/hashmap-containskey-method-in-java/?ref=lbp)
- [hashmap-get-method-in-java/?ref=lbp](https://www.geeksforgeeks.org/hashmap-get-method-in-java/?ref=lbp)
- [hashmap-values-method-in-java](https://www.geeksforgeeks.org/hashmap-values-method-in-java/?ref=lbp)
