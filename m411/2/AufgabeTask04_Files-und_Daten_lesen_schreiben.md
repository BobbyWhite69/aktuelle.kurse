### Aufgabe/Task: Nr. 04

Thema: Files lesen & schreiben

Geschätzter Zeitbedarf: 30-70 min pro Aufgabe

#### Aufgabenbeschreibung:

2.1 Textfiles einlesen mit BufferedReader

- [script2_1_Textfiles_einlesen_BufferedReader.pdf](./script2_1_Textfiles_einlesen_BufferedReader.pdf)

2.2 Textfiles einlesen BufferedReader oder Scanner

- [script2_2_Textfiles_einlesen_BufferedReader_o_Scanner.pdf](./script2_2_Textfiles_einlesen_BufferedReader_o_Scanner.pdf)

2.3 Strukturierte Textfiles einlesen

- [script2_3_Textfiles_einlesen_StrukturierteTextfiles_plusUebung.pdf](./script2_3_Textfiles_einlesen_StrukturierteTextfiles_plusUebung.pdf)

2.4 Textfiles schreiben

- [script2_4_Textfiles_schreiben_plusUebung.pdf](./script2_4_Textfiles_schreiben_plusUebung.pdf)

2.5 Zufallszahlen-Datei

Schreiben Sie ein einfaches Text-File. Der Inhalt sind ganzzahlige Zufallszahlen
zwischen 0 und 10000. Pro Zeile schreiben Sie eine Zahl. Programmieren Sie eine
Einstellung (Parameter), dass Sie wahlweise z.B. 500 oder 3'000’000
Zufallszahlen-Zeilen erzeugen können.

Zeigen Sie der Lehrperson jede Übung (entweder einzeln oder mehrere Übungen
zusammen) und zeigen Sie auch diese Zufallszahlen-Datei.


Bewertung: Keine, ist aber prüfungsrelevant.

Aufgaben / Übungen der Lehrperson zeigen
