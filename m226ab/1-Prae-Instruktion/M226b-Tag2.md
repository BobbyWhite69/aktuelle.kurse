# Präinstruktion M226b Tag 2

## Spielregel
	Beantworten Sie *schriftlich* und alleine für sich,
	die folgenden Fragen. Recherchieren Sie vorerst nicht!
	Vermutungen sind auch gut.
	
	Nach 15 min. können Sie im Internet
	oder in den Unterlagen recherchieren.
	Das Gespräch (Murmelrunde) mit dem/den
	Nachbarn ist in dieser Phase gut, hilfreich und erwünscht!

## Fragen
- Was ist eine Variable (auch Bezeichner oder 'identifier' genannt)? 
	<br>--> formulieren Sie das für eine:n Schüler:in der 2. Sek, ca. 13-jährig
	<br>(Java9, Kap 4.2)
- Was ist ein Konstruktor? Kann man eigene machen? Wann braucht man den?
- Was sind die 3 Möglichkeiten für Kommentare in Java? <br>(Java9, Kap. 4.3)
- Nennen Sie mindestens 5 "reservierte Wörter" in Java! <br>(Java9, Kap. 4.7)
- Wie geht ein "Casting" (explizite Typenkonversion) und wann macht man sowas? <br>(Java9, Kap. 4.9)
- Gibt es Konstruktoren, die man nicht sofort sehen kann? <br>(Java9, Kap. 7.1, 7.3)
- Wieviele Konstruktoren kann man für eine Klasse haben/machen?
- Wie wirkt Polymorphismus?

[Java9](../2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung/JAV9.pdf)

