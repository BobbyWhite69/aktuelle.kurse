package com.doerzbach;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedWriter;
import java.io.IOException;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CsvWriterTest {

    // TODO : Mock this to objects
    Sensor sensor1;
    BufferedWriter outBuffer;

    @Test
    void runTest() throws IOException {
        // Here we would like to test the CsvWriter and not the sensor or the Filewriter
        /* TODO: For this we should mocked the sensor1 and the outBuffer to be able to keep track of
                 number of times the sensor1 is called and the outBuffer is called. */
        CsvWriter w=new CsvWriter(1, outBuffer,sensor1,10);
        /* TODO: Here we should define the return values for getValue(), getUnit(), getName()
                 of sensor1    */
        // Now lets run the test
        w.run();
        /* TODO: Check how many times the sensors getXXX() methods were called.
                 It should be 10 times */
        /* TODO: check that the outBuffer is flushed 10 times */
        /* TODO: check that the outBuffer is written 10 times */
    }
}