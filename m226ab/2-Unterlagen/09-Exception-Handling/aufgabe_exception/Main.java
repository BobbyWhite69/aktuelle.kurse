/*  package xxx.yyyy; */
import java.io.*;
import java.util.Scanner;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        boolean read=false;
	    do{
            Scanner sc = new Scanner(System.in);
            System.out.print("Enter Filename to read:");
            String filename=sc.nextLine();
            try {
                Stream<String> lines=(new BufferedReader(new FileReader(new File(filename)))).lines();
                System.out.println("The file contains this:");
                lines.forEach(s -> System.out.println(s));
                read=true;
            } catch (FileNotFoundException e) {
                System.out.println("There was a FileNotFoundException when reading the file");
                System.out.println("Exception message was:"+e.getMessage());
                System.out.print("Stacktrace was:");
                e.printStackTrace(System.out);
            }
        } while (!read);
    }
}
