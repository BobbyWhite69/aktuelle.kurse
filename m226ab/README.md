[> **Modulidentifikation M226a** ](https://cf.ict-berufsbildung.ch/modules.php?name=Mbk&a=20101&cmodnr=226A&noheader=1)
<br>
[> **Modulidentifikation M226b** ](https://cf.ict-berufsbildung.ch/modules.php?name=Mbk&a=20101&cmodnr=226B&noheader=1)

# M226a - Klassenbasiert (ohne Vererbung) implementieren

### M226a LB1 (15%, mündliche Einzelprüfung, 12 min)
Themen: UML, OO-Prinzipien


### M226a LB2 (30%, schriftliche Prüfung, 60 min)
Themen: UML, OO-Prinzipien, ...


### M226a LB3 (55%, praktisches Projekt) 
Bewertungskriterien:<br>
Es müssen alle Elemente im Buch M226 von Ruggerio, Compendio von Kap. 5 bis 11, sowie Kap. 14 und 15 enthalten sein.
- Wer das Minimum des Kap. 13 macht, kann maximal die Note 4.7 erreichen.
- Wer ein eigenes Projekt "gut" abschliesst inkl. "Doku", "JavaDoc" und "JUnit-Tests", kann eine Note 6 machen. |


**Buch** [./2-Unterlagen/00-Buecher/Buch__M226_Ruggerio_Compendio](./2-Unterlagen/00-Buecher/Buch__M226_Ruggerio_Compendio/)


UML-Tools
- https://staruml.io/download
- https://umletino.com

<br>
<br>

| Tag  | Auftrag/Übung | Inhalte/Themen |
| ---- | ------------- | ------------------------ |
| 1    | [A11](./3-Auftraege-Uebungen/A11-Wissensaneignung1.md)            |  Modulvorstellung <br> Installation Eclipse oder ähnliche Programmierumgebung (Buch Kap. 12.1)<br> Beginn mit Buch/Skript Compendio 226 selbständig durchmachen (Teil A (Kap. 1-4))                     |
| 2    | [A12](./3-Auftraege-Uebungen/A12-WissensaneignungUML.md),[A13](./3-Auftraege-Uebungen/A12-GrundsaetzeDerOOProgrammierung.md)            |  Wissensaufbau mit Buch Compendio 226, selbständig durchmachen <br>*Input* JDK [Q](./2-Unterlagen/01-Einfuehrung-OOP-Klassen-Objekte/M226_Einfuehrung_JDK.pdf) & [A](./2-Unterlagen/01-Einfuehrung-OOP-Klassen-Objekte/M226_Einfuehrung_JDK_Anworten.pdf) <br>*Input* Klassen und Objekte [Q](./2-Unterlagen/01-Einfuehrung-OOP-Klassen-Objekte/M226_Klassen_Objekte.pdf) & [A](./2-Unterlagen/01-Einfuehrung-OOP-Klassen-Objekte/M226_Klassen_Objekte_loesungen.pdf)                                       |
| 3    |             |  Weiterarbeit am Wissensaufbau, <br>*Input* über UML-Zusammenhänge                                        |
| 4    |             |  **LB3** Definition eigenes Projektes (max 3 Pers)<br>(Projektumriss, Anforderungsdefinition, Activity-Diagram, Use-cases, ERM?, Class-Diagram, Sequence-Diagram)<br> **LB1** Ab dem 2. Teil des Halbtages laufend Kompetenzabnahmen/Basic-Check (mündlich einzeln, Teil A im Buch)  |
| 5    |             |  **LB1** Basic-Check (Fortsetzung) <br>Beginn mit dem eigenen Projekt (Planung/Konzept, UML). Lassen Sie sich von Kap. 13 inspirieren. Bedingung: Es müssen alle Elemente von Kap. 5 bis 11, sowie 14 und 15 enthalten sein.<br><br>Der LP die Aufgabenstellung aufzeigen. Diagramm(e) & Prosa   |
| 6    |             |  **LB2** Schriftliche Prüfung, 30% ModulNote<br>Weiterarbeit am Projekt                     |
| 7    |             |  Arbeit am Projekt<br>Präsentierung Zwischenstand des Projektes (v.a. eine Herausforderung)                     |
| 8    |             |  Arbeit am Projekt<br>Projektbeobachtung durch LP |
| 9    |             |  Arbeit am Projekt<br>Projektbeobachtung durch LP / erste Projektabnahmen  |
| 10   |             |  Arbeit am Projekt<br>Projektabschluss, Projektdemos<br>Projektbesprechung/Notengebung |

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

# M226b - Objektorientiert (mit Vererbung) implementieren


###  M226b LB1 (30%, schriftliche Prüfung, 60 min)
Themen: UML, OO-Prinzipien, Vererbung, Polymorphismus, JUnit-Tests

###  M226b LB2 (30%, Qualität und Quantität der Übungen)
Themen: Vererbung, Polymorphismus, JUnit-Tests, Anwendung Datenstrukturen & Algorithmen

###  M226b LB3 (40%, Pairprogramming-Miniprojekt
Thema: Selbstdefinition - Bearbeitungszeit 15-20 Std. (teilweise in Hausarbeit)


<br>
<br>**Buch** 
[./2-Unterlagen/00-Buecher/Java_9_Grundlagen_Programmierung](./2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung/JAV9.pdf)
<br>**dazugehörige(.java)** 
[./2-Unterlagen/00-Buecher/JAV9_Arbeitsdateien.zip](2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung/JAV9_Arbeitsdateien.zip)
<br>

| Tag  | Inhalte/Themen, Aufträge/Übungen |
| ---- | -------------------------------  |
| 1 - 18.11.21   | fällt aus (LKB)        |
| 2 - 25.11.21   | [Präinstruktion 1](1-Prae-Instruktion/M226b-Tag1.md)<br>[Einführung in JUnit-Tests](2-Unterlagen/07-Testen-(JUnit)/)<br>[Dynamisches Binden](https://de.wikipedia.org/wiki/Dynamische_Bindung) <br>Polymorphismus A25 [(.pdf)](3-Auftraege-Uebungen/A25-Inheritance_Polymorphism_Composition.pdf) [(.md)](3-Auftraege-Uebungen/A25-Inheritance_Polymorphism_Composition.md) oder [.docx](2-Unterlagen/04-Dynamische-Bindung-(Polymorphie)/Inheritance_Polymorphism_Composition) |
| 3 - 02.12.21   | [Präinstruktion 2](1-Prae-Instruktion/M226b-Tag2.md)<br>Wissensaufbau, Übungen, Training <br>[B22-Wissensaneignung2 (+ 2 Wissenstests)](3-Auftraege-Uebungen/B22-Wissensaneignung2.md)<br>[B23-KlassenAttributeMethoden](3-Auftraege-Uebungen/B23-KlassenAttributeMethoden.md) |
| 4 - 09.12.21   | [Präinstruktion 3](1-Prae-Instruktion/M226b-Tag3.md)<br>Wissensaufbau, Übungen, Training <br>[B24-Vererbung](3-Auftraege-Uebungen/B24-Wissensaneignung3Vererbung.md)  |
| 5 - 16.12.21   | Wissensaufbau, Übungen, Training <br>[Auswahl einer Aufgabenstellung](4-Aufgabenstellungen) |
| ---- | ---- Weihnachtsferien ----       |
| 6 - 06.01.22   | **LB1**, Start **LB3** |
| 7 - 13.01.22   | Arbeit an LB3          |
| 8 - 20.01.22   | Arbeit an LB3          |
| 9 - 27.01.22   | Arbeit an LB3          |
| 10 - 3.02.22   | Abgabe **LB3**         |
