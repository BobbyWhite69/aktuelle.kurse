# Aufgabe M152-h: Animationen erstellen (anim-CSS, anim-GIF, SVG, Keyframes, Transitions, Canvas)
Zeitbedarf: minimal 180 min, eher das Doppelte oder Dreifache


Übersicht über Techniken
- https://yalantis.com/blog/web-animation-technologies-and-tools


## 1.) Animationen mit CSS
1a.) Lesen Sie Kapitel 4 aus dem Buch (S. 98-121)

- [SaewertRiempp2019_Digital-Storytelling-im-Web](../x_gitressourcen/Buecher_Skripts_Praesentationen/SaewertRiempp2019_Digital-Storytelling-im-Web_9783658230555.pdf)



1b.) Schauen Sie sich das Video dieses Web-Entwicklers an 
- https://www.youtube.com/watch?v=AAsybyVjXss (11 min)


1c.) Und zeigen Sie auf **3 separaten** HTML-Seiten (oder .php) **je eine** der gezeigten Animationsarten und erklären Sie auf der selben Seite, wie diese funktionieren.
(machen Sie zunächst dafür mal eine eigene Seite z.B. "cssanimation.html" und verlinken Sie diese aus "index.html" her über ein Menü-Punkt)


## 2.) Animietes GIF
Nehmen Sie eine Bilderserie (ca 8-12 kleine aufeinanderfolgende Bilder / vorher mit Irfanview herunterrechnen damit es nicht zu gross wird) und erstellen Sie ein animirtes Gif. Es gibt verschiedene Tools dafür. Es gibt auch Videosoftware, die ein Video auf ein animiertes GIG herunterrechnen können.

- https://gif-erstellen.com
- https://gif-erstellen.online-umwandeln.de
- https://www.netzwelt.de/download/foto-grafik/animierte-gifs/index.html


*_Binden Sie alles in Ihre Webseite ein und beschreiben Sie kurz, wie Sie das gemacht/erstellt haben._*



## 3.) SVG
Studieren Sie die Links und versuchen Sie SVG für sich anzuwenden und zu dokumentieren auf Ihrer WebSite.

 - https://inkscape.org/de/release/inkscape-1.0
 - [Scalable Vector Graphics SVG](../x_gitressourcen/Animationen/SVG)
 - [Vergleich PNG - SVG](../x_gitressourcen/Animationen/SVG/Vergleich-PNG-SVG)
 - https://www.youtube.com/watch?v=ttwUKpzKYLw&list=PL055Epbe6d5bQubu5EWf_kUNA3ef_qbmL&index=6
 - https://wiki.selfhtml.org/wiki/SVG/Elemente/Grundformen
 - https://greensock.com/  https://greensock.com/cheatsheet

## 4.) Keyframes und Transitions
- https://developer.mozilla.org/de/docs/Web/CSS/@keyframes

## 5.) Canvas
- [HTML5 Canvas Bsp](../x_gitressourcen/Animationen/HTML5_Canvas)
- https://developer.mozilla.org/de/docs/Web/Guide/HTML/Canvas_Tutorial/Basic_animations
- https://james-priest.github.io/100-days-of-code-log-r2/CH12-Canvas.html
- https://wiki.selfhtml.org/wiki/JavaScript/Canvas

## 6.) Scroll-Animations
- [SaewertRiempp2019_Digital-Storytelling-im-Web S. 175 ff](../x_gitressourcen/Buecher_Skripts_Praesentationen/SaewertRiempp2019_Digital-Storytelling-im-Web_9783658230555.pdf)
- [Scroll-Animated Animation](../x_gitressourcen/Animationen/Scroll-Animated Animations)

