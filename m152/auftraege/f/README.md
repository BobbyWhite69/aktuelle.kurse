# Aufgabe M152-f: DSGVO, Cookie-Warnung, Impressum
Zeitbedarf: ca. 120-140 min plus 20 min Pause

1.) Lesen Sie zuerst die rechtlichen Grundlagen über die EU-Cookies-Richtlinie und die "DSGVO".

https://www.e-recht24.de/artikel/datenschutz/8451-hinweispflicht-fuer-cookies.html

https://www.k-webs.ch/news-cookiehinweis-auf-websites-in-der-schweiz

2.) Erstellen Sie auf ihrer "index.html" eine Cookie-Warnung inkl. der ‘erwarteten’ Funktionalität (*). Es steht Ihnen frei, einen Generator dafür zu benutzen. Im Internet finden Sie mehrere. Hier mal ein Anfang:

https://www.ithelps-digital.com/de/blog/webseiten/cookie-website-browser

https://www.cookiebot.com/de/cookie-warnung/


3.) Erstellen und verlinken Sie eine HTML-Seite, wo Sie die wichtigsten und/oder zentralen Elemente der DSGVO erklären (eigene Zusammenfassung). 


4.) Erstellen und verlinken Sie eine HTML-Seite, wo Sie die entwickelten Fragen und Antworten zum Urheberrecht präsentieren.


5.) Erstellen und verlinken Sie eine HTML-Impressum-Seite (oder PHP-Seite) mit allen "notwendigen" Angaben (Diese müssen aber stimmen, keine Fake-Angaben! Denn Ihre WebSite ist ja auch im Internet). 


6.) Laden Sie das alles auf Schultag-Ende auf Ihren WebSpace hoch.



Bemerkung (*): Wenn der User die Cookie-Warnung nicht quittiert, kann er auf der Webseite auch nicht alles (oder gar nichts?) sehen. Ausserdem darf die Cookie-Warnung beim zweiten Besuch der Webseite nicht nochmals aufpoppen. Also nur 1x pro User, bzw. 1x pro Browser.


[Auftrag und Links zu DSGVO_und_Cookies](../x_gitressourcen/DSGVO_und_Cookies) 

