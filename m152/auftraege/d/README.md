# Aufgabe M152-d: Urheberrecht
Zeitbedarf: ca. 45-60 min

**Lernziel**: Neben dem eigentlichen Aufarbeiten und lernen, was Urheberrecht ist und wie es sich in der Praxis auswirkt, werden Sie in dieser Aufgabe als Nebenkompezenz (und als methodisch-didaktische Variation) das hineinversetzen in eine andere Rollen üben können umd gleichzeitig lernen, die wichtigen Fragen zu stellen. --> 
_Lese- und Verständnisaufgabe über Urheberrecht_

 

**Ausgangslage**:

Stellen Sie sich vor, in einer Schweizer Sekundarschule oder in einem Gymnasium haben die 14-/15-jährigen Schüler in den letzten 2 Wochen wärend 6-8 Stunden "alles" über das Urheberrecht, Urhebergesetz, geistiges Eigentum gelernt. Sie sind Sachverständige/r für das Urheberrecht und müssen einen Test für diese Schüler bereitstellen, um zu sehen, ob sie die wichtigsten "Dinge" über diese Themen kennen.

 

**Vorgehen**:

Lesen/studieren Sie im unten angegebenen Link die Dokumente. Finden Sie aus möglichst allen Bereichen je 1-3 Fragen und geben Sie die richtigen Antworten dazu an. Dokumentieren Sie das in einer Seite in Ihrer WebSite gemäss und entsprechend im Stile Ihrer WebSite und Ihrem WebSite-Konzept.

 

**Erwartet werden**: 
 10-12 Fragen (und Antworten) 
- aus den verschiedenen Bereichen des Urheberrechts und des Urheberrechtsgesetzes (siehe Wikipedia), 
- des geistigen Eigentums und wie man etwas schützen kann (IGE-Website) 
- und die Vorgaben für das schweizerische Impressum (WEKA-Artikel)

 

**Es ist eine Einzelaufgabe**: 
Die Abgabe hat nicht 2x die gleichen Fragen & Antworten in der Klasse!


[Referenzmaterial](../x_gitressourcen/Urheberrecht)
