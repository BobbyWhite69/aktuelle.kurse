# Aufgabe M152-g: Bildformate, Videoformate, Soundformate erstellen und vergleichen
Zeitbedarf: ca. 60-90 min.

## 1.) Fotos/Bilder
Machen Sie zwei Fotos mit Ihrem Smartphone. Ein Foto hat möglichst wenige Details darauf (eine eintönige Wand, Fussboden, Fläche oder sowas).

Ein Foto hat viele Elemente darauf (z.B ein Bild aus Ihrem Fenster, ein Baum, ein Blumenbeet oder ähnliches)


- transferieren Sie die beiden Fotos auf Ihren Rechner.
- Speichern Sie neben dem originalen JPEG (unverändert) auch noch in die Dateiformate BMP, PNG-8, GIF sowie ein JPEG mit geringer Auflösung (kann man in den Softwaretools einstellen). *Wie weit kann man gehen?*
- Erstellen Sie eine HTML- oder PHP-Dokument und integrieren Sie die Fotos auf die WebSite in den verschiedenen Formaten und fügen Sie untenstehende Beschreibung(en) hinzu. 
- Notieren Sie zu den Fotos die Dateigrössen der Dateiformate JPEG (beide), BMP, PNG-8, GIF
- Erklären Sie mit eigenen Worten zu jedem Dateiformat, warum die Datei so gross oder klein ist.
- Erklären Sie auch Unterformate, falls es welche gibt (z.B. Interlaced, Transparenz, ...)
- Beschreiben Sie zu jedem Bildformat deren Vorteile und Nachteile sowie deren optimalen Einsatzzweck
 
Softwareempfehlung: https://www.irfanview.com/

Andere wären z.B. auch Gimp, Photoshp, ...

<br />https://onlinegiftools.com


## 2.) Videos
Erstellen Sie Mit Ihrem Smartphone ein Kurzvideo (ca. 10 Sekunden) transferieren Sie es auf Ihren Rechner und speichern Sie es in 3-4 verschiedenen "gängigen" Formaten ab. 
- Binden Sie das/die Video/s in 2-3 unterschiedlichen Arten in Ihre WebSite ein (Video-Tag in HTML5, oder mittels "embed" u.ä.)
- Erklären Sie mit eigenen Worten zu jedem Dateiformat, warum die Datei so gross oder klein ist.
- Erklären Sie auch Unterformate, falls es welche gibt und was sie bewirken.
- Beschreiben Sie zu jedem Video-Format deren Vorteile und Nachteile sowie deren optimalen Einsatzzweck

Softwareempfehlung: https://openshot.org


<br />https://mediaboom.com/
<br />https://www.softguide.de/software-tipps/videoformate
<br />https://www.nrwision.de/mitmachen/wissen/videoformate-vergleich
<br />https://www.netzsieger.de/ratgeber/videoformate
<br />https://blog.mynd.com/de/wichtigsten-videoformate



## 3.) Sounds/Töne/Musik/Jingle
Suchen Sie sich im Internet frei verfügbare Musik, Sounds oder Jingles und speichern Sie sie in 2-4 unterschiedlichen "gängigen" Formaten ab.



- Binden Sie die Audio-Dateien in Ihre Webseite auf 2-3 Arten (automatisches Losspielen) und als manuell startbares Audio entsprechend ein. 
- Erklären Sie mit eigenen Worten zu jedem Dateiformat, warum die Datei so gross oder klein ist.
- Erklären Sie auch Unterformate, falls es welche gibt.
- Beschreiben Sie zu jedem Audioformat deren Vorteile und Nachteile sowie deren optimalen Einsatzzweck


<br />https://www.audible.de
<br />https://de.wikipedia.org/wiki/Web_Audio_API
<br />https://googlechromelabs.github.io/web-audio-samples
<br />https://umaar.com/dev-tips/203-web-audio-inspector
<br />https://www.nrwision.de/mitmachen/wissen/audioformate-vergleich




**Hinweis:**
- erinnern Sie sich ans Modul 114 (1. Lehrjahr) und/oder holen sie sich dessen Unterlagen hervor
- lesen Sie sich im Internet schlau

