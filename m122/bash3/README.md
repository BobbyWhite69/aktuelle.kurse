
### Variablen 1

-   Variablen werden mit dem Zuweiseungsoperator `=` gesetzt.
-   Auf den Inhalt von Variablen kann mit `$` zugegriffen werden.
-   Der Inhalt einer Variable kann geändert werden

		[root@host /]# name="Hans"
		[root@host /]# echo $name
		Hans
		[root@host /]# name="Muster"
		[root@host /]# echo $name
		Muster

___


### Variablen 2

-   Die Ausgabe eines Befehls kann einer Variable zugewiesen werden
-   Der Befehl muss in `$( )` gesetzt werden
-   Der Inhalt von Variablen kann in anderen Befehlen weiterverwendet werden
-   Variablen können kopiert werden

		[root@host /]# datum=$(date +%Y_%m_%d)
		[root@host /]# echo $datum
		2022_10_06
		[root@host /]# touch file_$datum
		[root@host /]# ls
		file_2022_10_06
		[root@host /]# datum2=$datum; echo $datum2
		2022_10_06

___

### Ausgabe umleiten

Die Ausgabe eines Befehls kann umgeleitet werden mit `>` oder `>>`

Beispiele:
		ls -la > liste.txt
		./meinskript > outputofscript.txt
		cat outputofscript.txt >> list.txt
		`>>` hängt Inhalt an eine bestehende Datei an oder erstellt eine neue Datei, wenn sie noch nicht bestanden hatte 
		`>`  überschreibt den Inhalt komplett mit Neuem (erzeugt eine neue Datei)
