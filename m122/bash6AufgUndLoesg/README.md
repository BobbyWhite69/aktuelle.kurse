# Linux Bash Übungsaufgaben 1 & 2
mit  Beispiellösungen

**Aufgabe 1.1:**

Der Dateiname, welcher die Benutzernamen beinhaltet, wird mit `$1`
übergeben.

    #!/bin/bash
    for user in $(cat $1); do useradd $user; done

**Aufgabe 1.2:**

Der Dateiname, welcher die Gruppen beinhaltet, wird it `$1` übergeben,
der Benutzernamen mit `$2`.

    #!/bin/bash
    for group in $(cat $1); do groupadd $group $2; done

**Aufgabe 1.3:**

Der Benutzername wird mit `$1` übergeben

    #!/bin/bash
    name=$1_$(date '+%y-%m-%d').tar.gz;
    find / -user $1 -exec cp {} /root/found/ \; 
    tar -zcvf "/root/found/$name" /root/found/;
    find /root/found/ -type f ! -name "$name" -delete;

**Aufgabe 1.4:**

Das tool `fping` muss installiert sein (`apt-get install fping`).

    #!/bin/bash
    for i in $( ifconfig | grep "inet Adr" | grep -v "127.0.0.1" |
             cut -d ":" -f 2 | cut -d "." -f 1-3 ); do
         for k in $(seq 1 255); do
               fping -c 1 -t250 $i.$k 2>&1 |  grep " 0% " | cut -d " " -f 1 >ips.txt
         done
    done

    #alternative Lösung:
    fping -g -c 1 -t250 172.16.6.0/24 2>&1 | grep " 0% " | cut -d " " -f 1 ips.txt


**Aufgabe 2.1:**

    [root@host: ] mkdir /root/trash
    [root@host: ] touch /root/trash/file{1..10}
    [root@host: ] nano /root/trash.sh
    #!/bin/bash
    rm /root/trash/*
    [root@host: ] chmod +x trash.sh
    [root@host: ] crontab -e
    */5 * * * * /root/trash.sh
    [root@host: ] watch ls /root/trash  
    (Warten bis files verschwinden --erfolgreiche Ausführung)

**Aufgabe 2.2:**

IP wird als `$1` übergeben, *ban* oder *unban* als `$2`.

    #!/bin/bash
    if [ $2 = "ban" ]
     then
        echo "banning " $1
        iptables -A INPUT -s $1 -j DROP
    elif [ $2 = "unban" ]
     then
        echo "unbanning " $1
        iptables -D INPUT -s $1 DROP
    else
     echo "Verwendung:"
     echo "1.Arg: IP-Adresse" 
     echo "2.Arg.: ban oder unban"  
     echo "Beispiel: ./ban.sh 192.168.13.3 ban"
    fi

**Aufgabe 2.4:**

Setzen des SGID Bits (3 verschiedene Varianten):

    [root@host: ] chmod g+s /data/myFile 
    [root@host: ] chmod +s /data/myFile
    [root@host: ] chmod 2755 /data/myFile

Setzen des Sticky Bits (3 verschiedene Varianten):

    [root@host: ] chmod o+t /data 
    [root@host: ] chmod +t /data
    [root@host: ] chmod 1755 /data
