
# Shellprogrammierung

-   Linux bietet etliche Befehle, welche in der Shell direkt oder in
    einem Skript gebraucht werden können wie zB. `cp`, `alias`, `cat`,
    `rm`

-   Daneben bietet die Shell Programmierstrukturen wie zB. Schleifen,
    Verzweigungen, Variablen, usw.

-   Beide Elemente können in einem Skript verwendet werden, um Aufgaben
    auf dem System zu automatisieren

___


### Linux Verzeichnishierarchie

-   Der Verzeichnisbaum beginnt bei Linux beim Verzeichnis `/`
    (Vergleichbar mit `C:\` unter Windows)

-   Das Verzeichnis `/`wird auch Wurzel- oder Rootverzeichnis genannt

-   Verzeichnisse eine oder mehrere Hierarchiestufen weiter unten werden
    durch `/`-Zeichen getrennt

-   Beispiele:

        /etc/
        /usr/local/nginx
        /usr/bin/
        /home/user1
___


### Linuxprompt

-   Die Eingabeaufforderung unter Linux (auch prompt genannt) sieht zB.
    wiefolgt aus:

        user@host:/#

-   Der prompt bildet sich nach dem Schema:
    `<akt.username>@<hostname>:<akt.verzeichnis>#`

-   Das Tildezeichen `~` ist ein Kürzel für das Heimatverzeichnis des
    aktuellen Benutzers

-   Die Heimatverzeichnisse befinden sich i.d.R. unter
    `/home/<benutzername>`

-   Einzige Ausnahme: Das Heimatverzeichnis des Administratorusers
    (`root`) befindet sich unter `/root/`

___


### Systemspezifische Befehle

-   Der Befehl `reboot` (oder `shutdown -r` oder `init 6`) startet das
    System neu

-   Der Befehl `halt` (`shutdown -h`, `init 0` oder `poweroff`) schaltet
    das System ab

___


### Hilfe holen

-   Der Befehl `man` öffnet sie Hilfeseiten (manual) eines Befehls\
    Syntax: `man <Befehlsname>`

-   Der Befehl `apropos` durchsucht alle Hilfeseiten nach einem
    Stichwort\
    Syntax: `apropos <Stichwort>`

-   Der Befehl `which` findet den Ort eines installierten Programmes\
    Syntax: `which <Befehl>`

___


### Userspezifische Befehle

-   Der Befehl `whoami` zeigt den aktuellen Benutzernamen an

-   Der Befehl `who` zeigt alle am System angemeldeten Benutzer an

-   Der Befehl `groups` zeigt die Gruppen des aktuellen Benutzernamen an

-   Der Befehl `id` zeigt die Nutzerid und Gruppen des aktuellen
    Benutzers an

-   Der Befehl `su` wechselt den aktuellen Benutzer\
    Syntax: `su - <User>` (- sogt dafür, dass ins Heimverzeichnis der
    neuen Users gewechselt wird)

___


### Userspezifische Befehle

-   Der Befehl `useradd` fügt einen neuen Benutzer hinzu\
    Syntax: `useradd <User>`

-   Der Befehl `userdel` löscht einen bestehenden Benutzer\
    Syntax: `userdel <User>`

-   Der Befehl `passwd` kann (unter anderem) das Passwort wechseln\
    Syntax: `passwd <User>`

-   Der Befehl `logout` loggt den aktuellen Benutzer vom System aus
    (ebenso `exit`)

___


### Aliase

-   Aliase dienen als Nutzerspezifisches Kürzel für einen Befehl /
    Befehlskombination

-   Ein alias wird wiefolgt gesetzt:
    `alias <aliasname>="<befehl mit params/args>"`

-   Beispiel:

        user@host:/etc# alias gohome="cd ~"
        user@host:/etc# pwd
        /etc 
        user@host:/etc# gohome
        user@host:~# pwd
        /home/user

___


### Verzeichnisrelevante Befehle

-   Der Befehl `pwd` (*present work directory*) zeigt das aktuelle
    Verzeichnis an

-   Der Befehl `cd` ändert das aktuelle Verzeichnis.\
    Syntax: `cd <Zielverzeichnis>`

-   Der Befehl `mkdir` erstellt ein neues Verzeichnis.\
    Syntax: `mkdir <Verzeichnisname>`

-   Der Befehl `rmdir` lsöcht ein bestehendes Verzeichnis.\
    Syntax: `rmdir <Verzeichnisname>` (Verzeichnis muss leer sein!)

-   Der Befehl `ls` listet den Verzeichnisinhalt auf\
    Syntax: `ls <Verzeichnisname>`

___


### Dateirelevante Befehle

-   Der Befehl `cp` kopiert Dateien/Verzeichnisse\
    Syntax: `cp <Quelldatei> <Zieldatei>`\
    Syntax Verzeichnisse: `cp -R <Quellverzeichnis> <Zielverzeichnis>`

-   Der Befehl `rm` löscht Dateien/Verzeichnisse\
    Syntax: `rm <Zieldatei>`\
    Syntax Verzeichnisse: `rm -r <Zelverzeichnis>`

-   Der Befehl `mv` verschiebt (= umbenennen) Dateien/Verzeichnisse\
    Syntax: `mv <altedatei> <neuedatei>`

-   Der Befehl `touch` erstellt eine neue leere Datei\
    Syntax: `touch <Dateiname>`

-   Der Befehl `cat` gibt Dateinhalt aus\
    Syntax: `cat <Zieldatei>`

-   Der Befehl `wc` zählt Wörter oder Linien eines Dateiinhaltes\
    Syntax (linien): `wc -l <Zieldatei>` (-w zählt \# Wörter)

-   Der Befehl `echo` gibt eine Zeichenkette aus\
    Syntax: `echo "<Zeichenkette>"`

___
