# M122 - Bash - Prüfung 2 Serie b


**Erlaubte Hilfsmittel :**

-   Alle Hilfsmittel ausser Netzwerkkommunikation (**LAN-Kabel
    entfernen!**)

**Generell nicht erlaubt ist:**

-   Abschreiben von der Nachbarprüfung oder Weitergeben von
    Prüfungen/Prüfungsresultaten innerhalb der Klasse. Kommunikation
    über Handy oder Computernetzwerk.

Jeglicher Verstoss gegen diese Regeln führt umgehend zum Ausschluss aus
der Prüfung und der Note 1.\
Name: `__________________________` Klasse: `_____________` Datum:`_____________`


|Aufgabe   | max. Punkte | erreichte Punkte  |
|----      |----         | ---- |
|1a        |      1      |
|1b        |      1      |
|1c        |      1      |
|1d        |      1      |
|1e        |      1      |
|2a        |      2      |
|2b        |      2      |
|2c        |      2      |
|2d        |      2      |
|3a        |      3      |
|3b        |      5      |
|----      |----         |----
|Total     |     21      |



**Aufgabe - 1a (1 Punkt)** - grep - Gegeben ist folgendes Kommando:

``` {.bash frame="none" mathescape=""}
cat /etc/passwd | grep -v ssh
```

Erklären sie, was dieses Kommando genau macht (berücksichtigen sie auch
die Option `-v`):

Antwort:
`__________________________________________________________________________________ ___________________________________________________________________________________ ___________________________________________________________________________________`




**Aufgabe - 1b (1 Punkt)** - Verzeichnisse wechseln
  Angenommen, sie befinden sich auf Ihre Linuxsystem unter `/home/user1/`,
  geben sie die **realtive** und die **absolute** Pfadangabe an, um in das
  Verzeichnis `/tmp` zu wechseln:

  Antwort:
  `________________________________________________________________________________ _________________________________________________________________________________`




**Aufgabe - 1c (1 Punkt)** - Cronjob - Sie wollen eines Ihre Bashskripte mit crontab automatisieren,
dazu haben sie einen cronjob eingerichtet welcher nach nachfolgendem
Muster ausgelöst wird:

``` {.bash frame="none" mathescape=""}
0 2 * * 3
```

Wann wird das Skript jeweils ausgeführt?

Antwort:
`_________________________________________________________________________________ __________________________________________________________________________________ __________________________________________________________________________________`






**Aufgabe - 1d (+1 Punkt pro richtige, -1 Punkt pro fehlende/falsche Antwort)** - Welches Dateirecht muss zusätzlich auf einer Skriptdatei gesetzt
werden, damit diese ausgeführt werden kann? (einkreisen)

		- `0`
		- `x`
		- `w`
		- `t`
		- `s`

**Aufgabe - 1e (1 Punkt)** - Wildcards - Sie führen folgendes Kommando
aus:

``` {.bash frame="none" mathescape=""}
touch {d..f}file
```

Erklären sie, was das Kommando genau macht:

Antwort:
`___________________________________________________________________________________ ____________________________________________________________________________________ ____________________________________________________________________________________`\


## Teil 2 - Codestücke

**Aufgabe - 2a (2 Punkte)** - Backup Skript

Nachfolgend sehen sie ein unvollständiges Backupskript. Die wichtigsten
Informationen, um mit `tar` ein Backup durchzuführen, werden in den
Variablen `quelle`, `ziel` und `outputFile` zwischengespeichert.
Vervollständigen sie das Skript so, dass ein Backup vom Ordner `home`
gemacht wird und zB. in der Datei `/var/backups/home-20151214.tgz`
abgelegt wird. Benutzen sie dazu die bereits definierten Variablen.

``` {.bash frame="none" mathescape=""}
#!/bin/bash        
outputFile=home-$(date +%Y%m%d).tgz  
quelle="/home/"
ziel="/var/backups/"
tar -cZf ________________ __________
```

- for in

Nachfolgend sehen sie ein unvollständiges Skript. Sie sollten das Skript
so ergänzen, dass es den Inhalt des Verzeichnisses `/home/` ausgibt und
zwar unter der Verwendung der abgebildeten `for in` Schleife.

``` {.bash .numberLines linenos="true" frame="none" mathescape=""}
#!/bin/bash
for i in ______________ ; do
        echo $i
done
```

- If - else

Nachfolgend sehen sie ein unvollständiges Skript. Sie sollen das Skript
nun so ergänzen (Zeilen 3 und 5), dass dieses, falls das 1. Argument den
Wert `keyw0rd` hat, den Text *sie dürfen passieren* ausgibt und falls
nicht den Text *nicht erlaubt!*.

``` {.bash .numberLines linenos="true" frame="none" mathescape=""}
#!/bin/bash
if [ $1 = "keyw0rd" ]; then
    _____________________
else
    _____________________
fi
```

- while

Nachfolgend sehen sie ein unvollständiges Skript. Sie sollen das Skript
so ergänzen, dass die Schleife solange läuft, wie `zahler` kleiner als
10 ist (Zeile 3) und der `zaehler` zum Schluss jedes Schleifendurchlaufs
um 1 erhöht wird (Zeile 5).

``` {.bash .numberLines linenos="true" frame="none" mathescape=""}
#!/bin/bash 
zaehler=0
while [  $zaehler _________ ]; do
    echo der zaehler ist $zaehler
    ____________
done
```


- Rechnerskript

Im nachfolgenden sehen sie ein Taschenrechnerskript, welches zwei Zahlen
und eine Operation einliest und dann die beiden Zahlen mit dieser
Operation verrechnet (zB. addiert) und ausgibt:

``` {.bash .numberLines linenos="true" frame="none" mathescape=""}
echo "bitte gebe eine Nummer ein"
read n1
echo "bitte wähle eine Operation"
echo "1. addieren"
echo "2. subtrahieren"
echo "3. dividieren"
echo "4. multiplizieren"
read opr
echo "bitte gebe eine zweite Nummer ein"
read n2

if [ $opr = "1" ]
   then
      echo $((n1+n2))
___________
    ___________
      ________________         
elif [ $opr = "3" ]
   then
      echo $((n1/n2))
elif [ $opr = "4" ]
   then
       echo $((n1*n2))
fi
```

Der Code einer Operation fehlt (Subtraktion). Ergänzen sie den Code an
der hervorgehobenen Stelle so, dass auch die Subtraktion funktioniert.\


- IP-Adressen von Nameservern

In der Datei `/etc/resolv.conf` sind die IPs der von Ihrem Linuxsystem
verwendeten Nameserver eingetragen. Der Inhalt dieser Datei kann zB. so
aussehen:

		search example.com
		nameserver 10.0.1.6
		nameserver 10.0.1.7

-- Schreiben sie ein kleines Skript, welches alle IP's der Nameserver
ausgibt. Mit dem obigen Beispiel sollte Ihr Skript also folgende Ausgabe
erzeugen:
	

		10.0.1.6
		10.0.1.7

Verwenden sie dazu `cat`, `grep` und `cut`.

