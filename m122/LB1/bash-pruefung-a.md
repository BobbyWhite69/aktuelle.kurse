# M122 - Bash - Prüfung 2 Serie a


**Erlaubte Hilfsmittel :**

-   Alle Hilfsmittel ausser Netzwerkkommunikation (**LAN-Kabel
    entfernen!**)

**Generell nicht erlaubt ist:**

-   Abschreiben von der Nachbarprüfung oder Weitergeben von
    Prüfungen/Prüfungsresultaten innerhalb der Klasse. Kommunikation
    über Handy oder Computernetzwerk.

Jeglicher Verstoss gegen diese Regeln führt umgehend zum Ausschluss aus
der Prüfung und der Note 1.
Name: `__________________________` Klasse: `_____________` Datum:`_____________`


|Aufgabe   | max. Punkte | erreichte Punkte  |
|----      |----         | ---- |
|1a        |      1      |
|1b        |      1      |
|1c        |      1      |
|1d        |      1      |
|1e        |      1      |
|2a        |      2      |
|2b        |      2      |
|2c        |      2      |
|2d        |      2      |
|3a        |      4      |
|3b        |      4      |
|----      |----         |----
|Total     |     21      |


**Aufgabe - 1a (1 Punkt)** - Cronjob

Sie wollen eines Ihre Bashskripte mit crontab automatisieren,
dazu haben sie einen cronjob eingerichtet welcher nach nachfolgendem
Muster ausgelöst wird:

``` {.bash frame="none" mathescape=""}
0 4 * * 1
```
Wann wird das Skript jeweils ausgeführt?

Antwort:
`_________________________________________________________________________________ __________________________________________________________________________________ __________________________________________________________________________________`



**Aufgabe - 1b (1 Punkt)** - Wildcards 

Sie führen folgendes Kommando aus:

``` {.bash frame="none" mathescape=""}
touch {a..c}file
```

Erklären sie, was das Kommando genau macht:

Antwort:
`_________________________________________________________________________________ __________________________________________________________________________________ __________________________________________________________________________________`



**Aufgabe - 1c (1 Punkt)** - grep 

Gegeben sei folgendes Kommando:

``` {.bash frame="none" mathescape=""}
cat /etc/passwd | grep -v root
```

Erklären sie, was dieses Kommando genau macht (berücksichtigen sie auch
die Option `-v`):

Antwort:
`_________________________________________________________________________________ __________________________________________________________________________________ __________________________________________________________________________________`



**Aufgabe - 1d (1 Punkt)** - Verzeichnisse wechseln


Angenommen, sie befinden sich auf Ihre Linuxsystem unter `/root/`, geben
sie die **realtive** und die **absolute** Pfadangabe an, um in das
Verzeichnis `/tmp` zu wechseln:

Antwort:
`_________________________________________________________________________________ __________________________________________________________________________________ __________________________________________________________________________________`



**Aufgabe - 1e (+1 Punkt pro richtige, -1 Punkt pro fehlende/falsche Antwort)** - Dateirechte

Welches Dateirecht muss zusätzlich auf einer Skriptdatei gesetzt
werden, damit diese *ausgeführt* werden kann? (einkreisen)

		- `0`
		- `x`
		- `w`
		- `t`
		- `s`



## Teil 2 - Codestücke

**Aufgabe - 2a (2 Punkte)** - If - else\
\
Nachfolgend sehen sie ein unvollständiges Skript. Sie sollen das Skript
nun so ergänzen (Zeilen 3 und 5), dass dieses, falls das 1. Argument den
Wert `passw0rd` hat, den Text *Passwort korrekt, Zutritt erlaubt*
ausgibt und falls nicht den Text *Zutritt verweigert!*.

``` {.bash .numberLines linenos="true" frame="none" mathescape=""}
#!/bin/bash
if [ $1 = "passw0rd" ]; then
    _____________________
else
    _____________________
fi
```


**Aufgabe - 2b (2 Punkte)** - for in

Nachfolgend sehen sie ein unvollständiges Skript. Sie sollten das Skript
so ergänzen, dass es den Inhalt des Verzeichnisses `/etc/` ausgibt und
zwar unter der Verwendung der abgebildeten `for in` Schleife.

``` {.bash .numberLines linenos="true" frame="none" mathescape=""}
#!/bin/bash
for i in ______________ ; do
        echo $i
done
```


**Aufgabe - 2c (2 Punkte)** - while

Nachfolgend sehen sie ein unvollständiges Skript. Sie sollen das Skript
so ergänzen, dass die Schleife solange läuft, wie `zahler` grösser als
10 ist (Zeile 3) und der `zaehler` zum Schluss jedes Schleifendurchlaufs
um 1 reduziert wird (Zeile 5).

``` {.bash .numberLines linenos="true" frame="none" mathescape=""}
#!/bin/bash 
zaehler=20
while [  $zaehler _________ ]; do
    echo der zaehler ist $zaehler
    ____________
done
```

**Aufgabe - 2d (2 Punkte)** - Backup Skript

Nachfolgend sehen sie ein unvollständiges Backupskript. Die wichtigsten
Informationen, um mit `tar` ein Backup durchzuführen, werden in den
Variablen `source`, `target` und `outputFile` zwischengespeichert.
Vervollständigen sie das Skript so, dass ein Backup vom Ordner `home`
gemacht wird und zB. in der Datei `/var/backups/home-20151214.tgz`
abgelegt wird. Benutzen sie dazu die bereits definierten Variablen.

``` {.powershell frame="none" mathescape=""}
#!/bin/bash          
source="/home/"
target="/var/backups/"
outputFile=home-$(date +%Y%m%d).tgz
tar -cZf ________________ __________
```



## Teil 3 - Programmieraufgaben

**Aufgabe - 3a (4 Punkte)** - IP-Adressen von Nameservern\
\
In der Datei `/etc/resolv.conf` sind die IPs der von Ihrem Linuxsystem
verwendeten Nameserver eingetragen. Der Inhalt dieser Datei kann zB. so
aussehen:

```
		search example.com
		nameserver 10.0.3.4
		nameserver 10.0.3.5
```

Schreiben sie ein kleines Skript, welches alle IP's der Nameserver
ausgibt. Mit dem obigen Beispiel sollte Ihr Skript also folgende Ausgabe
erzeugen:

```
		10.0.3.4
		10.0.3.5
```
Verwenden sie dazu `cat`, `grep` und `cut`.\


**Aufgabe - 3b (4 Punkte, 1 Punkt pro Zeile)** - Rechnerskript

Im nachfolgenden sehen sie ein Taschenrechnerskript, welches zwei Zahlen
und eine Operation einliest und dann die beiden Zahlen mit dieser
Operation verrechnet (zB. addiert) und ausgibt:

``` {.bash .numberLines linenos="true" frame="none" mathescape=""}
echo "Geben sie die erste Nummer ein:" 
read a 
echo "Geben sie die zweite Nummer ein:" 
read b 
echo "Geben sie die gewünschte Opertaion +, - , / oder x ein" 
    read opr 
if [ $opr = "+" ] 
  then 
  op=`expr $a + $b`
  echo "$op"
    _______________
    _______________
    _______________
    _______________
elif [ $opr = "/" ] 
  then 
  op=`expr $a / $b`
  echo "$op"
elif [ $opr = "x" ] 
then 
  op=`expr $a \* $b`
  echo "$op"
fi 
```

Der Code einer Operation fehlt (Subtraktion). Ergänzen sie den Code an
der hervorgehobenen Stelle so, dass auch die Subtraktion funktioniert.
