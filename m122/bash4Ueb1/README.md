# Übungsaufgaben Skripting

 1.  Erzeugt Benutzer anhand einer Liste von Benutzernamen in einer
     Textdatei (via Parameter angegebenen).
     Hinweis: Benutzen sie `useradd` und `cat`.

 2.  Fügt einen Benutzer anhand einer Liste von Gruppen in einer
     Textdatei (via Parameter angegebenen) den jeweiligen Gruppen
     hinzu.
     Hinweis: Benutzen sie `groupadd` und `cat`.

 3.  Findet alle Dateien, welche einem (via Parameter angegebenen)
     Benutzer gehören und kopiert diese an den aktuellen Ort. Die
     kopierten Dateien werden zu einem `tar.gz` Archiv zusammengefasst
     und danach gelöscht. Die Archivdatei wird mit dem Benutzernamen
     und dem aktuellen Datum benannt.
     Hinweis: Benutzen sie `find`, `tar`, `rm` und `date`.

 4.  Ermittelt die eigene IP-Adresse und macht einen PING-Sweep für das
     Subnetz der eigenen IP. Gibt aus, welche Hosts up sind und
     speichert die IP-Adressen der Hosts in einer Textdatei.
     Hinweis: Benutzen sie `ping` (oder `fping`), `ifconfig` und
     `grep`.

 5.  Ermittelt die events der Stadt Zürich für das aktuellen Datum von
     usgang.ch. Erweitern sie das Skript danach auf beliebige Städte
     (unter usgang.ch gelistete) und die Angabe eines Datums (wenn kein
     Datum angegeben wird, wird das aktuelle angewendet).
     Hinweis: Benutzen sie `curl`, `grep` und `cut`. Der erste, der ein
     funktionierendes Skript für diese Aufgabe einsendet, gewinnt
     \"Gipfeli und Schoggistengeli\".

Als Informationsquelle dient folgendes Onlinebuch:
<http://openbook.rheinwerk-verlag.de/shell_programmierung/>
