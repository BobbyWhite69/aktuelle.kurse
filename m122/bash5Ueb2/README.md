# Übungsaufgaben 2 Skripting


 1.  Erstellen sie einen Ordner `/root/trash` und erzeugen sie einige
     Dateien darin. Erstellen sie ein Skript, welches alle 5 Minuten
     die Dateien innerhalb von diesem Ordner löscht (für Infos siehe
     auch Link 3 im Anhang). Überprüfen sie, ob ihr Skript korrekt
     eingerichtet ist, indem sie nachsehe, ob die Files nach 5 Minuten
     gelöscht wurden.

 2.  Erstellen sie ein Skript, mit welchem eine IP-Adressrange *bannen*
     oder *unbannen* können. Es gibt unterschiedliche tools, womit sie
     diese Funktionalität umsetzen können. Verwenden sie das Internet
     zur Informationssuche.

 3.  Erstellen sie folgende Benutzer und Gruppen. Benutzen sie zur
     Automatisierung die Skripte aus Bash Uebungsaufgaben 1. Versuchen
     sie den Prozess der Erstellung möglichst stark zu automatisieren:

     
     ![image](img/u1.png){height="3.8in"}
     

 4.  Erstellen sie folgende Ordnerstruktur und setzen sie die
     abgebildeten Berechtigungen (Auf den Berechtigungen ist auch das
     SGID-Bit (`s`) und sticky-Bit (`T`) abgebildet. Setzen sie auch
     dieses. Sie finden eine Erklärung und Anleitung im zweiten Link
     zuunterst in diesem Übungsblatt.) :

     
     ![image](img/u2.png){height="1.8in"}
     

<http://linux-infopage.de/show.php?page=berichte-berechtigungen>\
<http://www.zettel-it.de/docs/SUID-SGID-und-Sticky-Bit.pdf>\
<https://www.howtoforge.de/anleitung/eine-kurze-einfuhrung-in-cron-jobs/>\
<http://openbook.rheinwerk-verlag.de/shell_programmierung/>
